"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_sequences admin *

:details: lara_django_sequences admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev admin_generator lara_django_sequences >> admin.py" to update this file
________________________________________________________________________
"""
# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import ExtraData, SequenceClass, Sequence


@admin.register(ExtraData)
class ExtraDataAdmin(admin.ModelAdmin):
    list_display = (
        'data_type',
        'namespace',
        'URI',
        'text',
        'XML',
        'JSON',
        'bin',
        'media_type',
        'IRI',
        'URL',
        'description',
        'extra_data_id',
        'image',
        'file',
    )
    list_filter = ('data_type', 'namespace', 'media_type')


@admin.register(SequenceClass)
class SequenceClassAdmin(admin.ModelAdmin):
    list_display = ('class_id', 'name', 'description')
    search_fields = ('name',)


@admin.register(Sequence)
class SequenceAdmin(admin.ModelAdmin):
    list_display = (
        'sequence_id',
        'namespace',
        'name',
        'name_full',
        'UUID',
        'hash_SHA256',
        'sequence_class',
        'sequence',
        'media_type',
        'sequence_file',
        'status',
        'description',
        'URI',
        'IRI',
    )
    list_filter = ('namespace', 'sequence_class', 'media_type', 'status')
    raw_id_fields = ('tags',)
    search_fields = ('name',)
