"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_sequences gRPC handlers*

:details: lara_django_sequences gRPC handlers.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

from django_socio_grpc.utils.servicer_register import AppHandlerRegistry
#from .services import Service


def grpc_handlers(server):
    """ """
    app_registry = AppHandlerRegistry("lara_django_sequences", server)
    # app_registry.register(Service)
