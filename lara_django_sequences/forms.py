"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_sequences admin *

:details: lara_django_sequences admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev forms_generator -c lara_django_sequences > forms.py" to update this file
________________________________________________________________________
"""
# django crispy forms s. https://github.com/django-crispy-forms/django-crispy-forms for []
# generated with django-extensions forms_generator -c  [] > forms.py (LARA-version)

from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column

from .models import ExtraData, SequenceClass, Sequence 

class ExtraDataCreateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = (
                'data_type',
                'namespace',
                'URI',
                'text',
                'XML',
                'JSON',
                'media_type',
                'IRI',
                'URL',
                'description',
                'image',
                'file')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'data_type',
                'namespace',
                'URI',
                'text',
                'XML',
                'JSON',
                'media_type',
                'IRI',
                'URL',
                'description',
                'image',
                'file',
            Submit('submit', 'Create')
        )

class ExtraDataUpdateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = (
                'data_type',
                'namespace',
                'URI',
                'text',
                'XML',
                'JSON',
                'media_type',
                'IRI',
                'URL',
                'description',
                'image',
                'file')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'data_type',
                'namespace',
                'URI',
                'text',
                'XML',
                'JSON',
                'media_type',
                'IRI',
                'URL',
                'description',
                'image',
                'file',
            Submit('submit', 'Update')
        )

class SequenceClassCreateForm(forms.ModelForm):
    class Meta:
        model = SequenceClass
        fields = (
                'name',
                'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'name',
                'description',
            Submit('submit', 'Create')
        )

class SequenceClassUpdateForm(forms.ModelForm):
    class Meta:
        model = SequenceClass
        fields = (
                'name',
                'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'name',
                'description',
            Submit('submit', 'Create')
        )

class SequenceCreateForm(forms.ModelForm):
    class Meta:
        model = Sequence
        fields = (
                'namespace',
                'name',
                'name_full',
                'UUID',
                'hash_SHA256',
                'sequence_class',
                'sequence',
                'media_type',
                'sequence_file',
                'status',
                'description',
                'URI',
                'IRI')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name',
                'name_full',
                'UUID',
                'hash_SHA256',
                'sequence_class',
                'sequence',
                'media_type',
                'sequence_file',
                'status',
                'description',
                'URI',
                'IRI',
            Submit('submit', 'Create')
        )

class SequenceUpdateForm(forms.ModelForm):
    class Meta:
        model = Sequence
        fields = (
                'namespace',
                'name',
                'name_full',
                'UUID',
                'hash_SHA256',
                'sequence_class',
                'sequence',
                'media_type',
                'sequence_file',
                'status',
                'description',
                'URI',
                'IRI')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name',
                'name_full',
                'UUID',
                'hash_SHA256',
                'sequence_class',
                'sequence',
                'media_type',
                'sequence_file',
                'status',
                'description',
                'URI',
                'IRI',
            Submit('submit', 'Update')
        )

# from .forms import ExtraDataCreateForm, SequenceClassCreateForm, SequenceCreateFormExtraDataUpdateForm, SequenceClassUpdateForm, SequenceUpdateForm
