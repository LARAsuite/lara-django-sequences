"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_sequences models *

:details: lara_django_sequences database models.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - remove unwanted models/fields
________________________________________________________________________
"""

import uuid

from django.conf import settings
from django.utils.translation import gettext_lazy as _

from django.db import models

from lara_django_base.models import DataType, MediaType, ExtraDataAbstr, Tag, Namespace, ItemStatus


settings.FIXTURES += []


class ExtraData(ExtraDataAbstr):
    """This class can be used to extend the Sequence data, by extra information, 
       e.g.,  properties relevant only for certain sequences ... """
    extra_data_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    image = models.ImageField(upload_to='sequences/images', blank=True, default="image.svg",
                              help_text="extra sequence images")
    file = models.FileField(
        upload_to='sequences/', blank=True, null=True, help_text="rel. path/filename")


class SequenceClass(models.Model):
    """ classes  of sequences, like  DNA, RNA, PNA, XNA, peptide/protein ... """
    class_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(
        unique=True, help_text="classes  of sequences, like  DNA, RNA, PNA, XNA, peptide/protein ..")
    description = models.TextField(
        blank=True, help_text="description of the sequence class")

    def __str__(self):
        return self.name or ""

    class Meta:
        verbose_name_plural = 'SequenceClasses'


class Sequence(models.Model):
    """ Sequenc class describing, sequences of DNA, RNA, PNA, XNA, peptide/protein ... """
    sequence_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    namespace = models.ForeignKey(Namespace, related_name='%(app_label)s_%(class)s_namespaces_related',
                                  related_query_name="%(app_label)s_%(class)s_namespaces_related_query",
                                  on_delete=models.CASCADE, null=True, blank=True,
                                  help_text="namespace of sequence")
    name = models.TextField(help_text="short sequence name")
    name_full = models.TextField(
        blank=True, null=True, help_text="long sequence name")
    UUID = models.UUIDField(default=uuid.uuid4, help_text="LARA sequence UUID")
    hash_SHA256 = models.CharField(max_length=256, blank=True, null=True,
                                   help_text="SHA256 hash of item for identity/dublicate checking")
    sequence_class = models.ForeignKey(SequenceClass, related_name="%(app_label)s_%(class)s_sequence_class_related",
                                       related_query_name="%(app_label)s_%(class)s_sequence_class", on_delete=models.CASCADE, blank=True,
                                       help_text="sequence class, like  DNA, RNA, PNA, XNA, peptide/protein ... ")
    sequence = models.TextField(
        blank=True, help_text="sequence can be any supported format, defined by substance (polymer) file type")
    # ~ source_organism = models.ForeignKey(Organism, on_delete=models.CASCADE, blank=True,
    # ~ help_text="organism from which the sequence originated - important for codon usage")
    media_type = models.ForeignKey(MediaType, related_name="%(app_label)s_%(class)s_extra_data_related",
                                   on_delete=models.CASCADE, null=True, blank=True,  help_text="file type of sequence data (file)")
    sequence_file = models.FileField(
        upload_to='sequences/', blank=True, null=True, help_text="rel. path/filename")


    status = models.ForeignKey(ItemStatus, related_name="%(app_label)s_%(class)s_status_related",
                                 on_delete=models.CASCADE, null=True, blank=True,  help_text="status of sequence, e.g., selected, synchronised ... ")
    tags = models.ManyToManyField(Tag, blank=True, help_text="tags")
    description = models.TextField(
        blank=True, null=True, help_text="description of the sequence")
    URI = models.TextField(blank=True,  null=True, max_length=512,
                           help_text="gneneric URI to the data")
    IRI = models.URLField(
        blank=True,  null=True, unique=True, max_length=512, help_text="International Resource Identifier - IRI: is used for semantic representation ")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""

    # custom save routine
    # ~ def save(self, *args, **kwargs): #def save(self, force_insert=None, using=None): #force_insert=force_insert, using=using
        #~ """ """
        # ~ # do some custom saves here ...
        #~ super().save(*args, **kwargs)

    class Meta:
        pass
        # ~ verbose_name = 'ForumContribution'
        # ~ verbose_name_plural = 'ForumContributions'
        #db_table = "lara_metainfo_item_class"
