"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_sequences admin *

:details: lara_django_sequences admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev tables_generator lara_django_sequences >> tables.py" to update this file
________________________________________________________________________
"""
# django Tests s. https://docs.djangoproject.com/en/4.1/topics/testing/overview/ for lara_django_sequences
# generated with django-extensions tests_generator  lara_django_sequences > tests.py (LARA-version)

import logging
import django_tables2 as tables


from .models import ExtraData, SequenceClass, Sequence

class ExtraDataTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_sequences:extradata-detail', [tables.A('pk')]))

    class Meta:
        model = ExtraData

        fields = (
                'data_type',
                'namespace',
                'URI',
                'text',
                'XML',
                'JSON',
                'bin',
                'media_type',
                'IRI',
                'URL',
                'description',
                'image',
                'file')

class SequenceClassTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_sequences:sequenceclass-detail', [tables.A('pk')]))

    class Meta:
        model = SequenceClass

        fields = (
                'name',
                'description')

class SequenceTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_sequences:sequence-detail', [tables.A('pk')]))

    class Meta:
        model = Sequence

        fields = (
                'namespace',
                'name',
                'name_full',
               
               
                'sequence_class',
                'sequence',
                'media_type',
                'sequence_file',
                'status',
                'description',
              
                )

