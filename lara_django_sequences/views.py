"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_sequences views *

:details: lara_django_sequences views module.
         - add app specific urls here
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: -
________________________________________________________________________
"""

from dataclasses import dataclass, field
from typing import List

from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView

from django_tables2 import SingleTableView

from .models import Sequence
from .forms import SequenceCreateForm, SequenceUpdateForm
from .tables import SequenceTable

# Create your  lara_django_sequences views here.


@dataclass
class SequencesMenu:
    menu_items:  List[dict] = field(default_factory=lambda: [   
        {'name': 'Sequences',
         'path': 'lara_django_sequences:sequence-list'},
    ])

class SequenceSingleTableView(SingleTableView):
    model = Sequence
    table_class = SequenceTable

    #fields = ('name', 'name_full', 'URL', 'handle', 'IRI', 'manufacturer', 'model_no', 'product_type', 'type_barcode', 'product_no', 'weight', 'specifications', 'spec_JSON', 'spec_doc', 'brochure', 'quickstart', 'manual', 'service_manual', 'icon', 'image', 'description', 'sequence_id', 'sequence_class', 'shape')

    template_name = 'lara_django_sequences/list.html'
    success_url = '/sequences/sequence/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Sequence - List"
        context['create_link'] = 'lara_django_sequences:sequence-create'
        context['menu_items'] = SequencesMenu().menu_items
        return context


class SequenceDetailView(DetailView):
    model = Sequence

    template_name = 'lara_django_sequences/sequence_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Sequence - Details"
        context['update_link'] = 'lara_django_sequences:sequence-update'
        context['menu_items'] = SequencesMenu().menu_items
        return context


class SequenceCreateView(CreateView):
    model = Sequence

    template_name = 'lara_django_sequences/create_form.html'
    form_class = SequenceCreateForm
    success_url = '/sequences/sequence/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Sequence - Create"
        return context


class SequenceUpdateView(UpdateView):
    model = Sequence

    template_name = 'lara_django_sequences/update_form.html'
    form_class = SequenceUpdateForm
    success_url = '/sequences/sequence/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Sequence - Update"
        context['delete_link'] = 'lara_django_sequences:sequence-delete'
        return context


class SequenceDeleteView(DeleteView):
    model = Sequence

    template_name = 'lara_django_sequences/delete_form.html'
    success_url = '/sequences/sequence/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Sequence - Delete"
        context['delete_link'] = 'lara_django_sequences:sequence-delete'
        return context
