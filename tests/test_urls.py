"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_sequences tests *

:details: lara_django_sequences application urls tests.
         - 
:authors: mark doerr  <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

from django.test import TestCase
from django.urls import resolve, reverse

# from lara_django_sequences.models import

# Create your lara_django_sequences tests here.
